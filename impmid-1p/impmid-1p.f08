!gfortran impmid.f08 -o impmid -cpp -DFULL_TYPE=real128 -DREDU_TYPE=real32
! Full and reduced precision type defaults
#ifndef FULL_TYPE
#define FULL_TYPE real64
#endif
#ifndef REDU_TYPE
#define REDU_TYPE real32
#endif

program main
	!Declare the full and reduced precision type names.
	!remember for literal constants to put 1.0_fp to cast correctly
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE, qp=>real128
	implicit none
	character(len=32) :: arg
	real(fp) :: dt, ti=0.0, tf=10.0, dx, xi=0.0, xf=0.5
	real :: start, finish !start/finish times for method
	real(fp), allocatable :: us(:, :)
	real(qp), allocatable :: ue(:)
	integer(kind=16) :: nx, nt 

	!get command line args and then convert
	!https://gcc.gnu.org/onlinedocs/gfortran/GETARG.html
	!https://riptutorial.com/fortran/example/26615/passing-command-line-arguments
	call getarg(1, arg)
	read(arg, *)dt 
	!print *, dt
	call getarg(2, arg)
	read(arg, *)dx

	if (sizeof(tf) == 16) then
		nt = (tf - ti) / dt 
		nx = (xf - xi) / dx
	else
		nt = ceiling((tf - ti) / dt)
		nx = ceiling((xf - xi) / dx)
	end if

	allocate(us(2, nx))
	allocate(ue(nx))
	
	call cpu_time(start)
	call imp_mid(ti, tf, dt, dx, nx, nt, us)
	call cpu_time(finish)
	print *, "method duration: ", finish-start
	
	open(unit=2, file="../ref_solution/ref_sol.txt")
	read (2,*) ue
	print *, "method error: ", norm2(ue(:) - real(us(1, :), qp)) / sqrt(real(nx, qp))
	!print *, us(1, :)

!Why this contains statement? see last answer and comments of
!https://stackoverflow.com/questions/16741252/function-return-type-mismatch
!links to useful
!http://www.pcc.qub.ac.uk/tec/courses/f77tof90/stu-notes/f90studentMIF_4.html
contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Full Precision Problem Functions
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
function init_cond(n) result(uxi)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	integer(kind=16), intent(in) :: n
	real(fp) :: uxi(n)
	uxi(1) = 323.0_fp
	uxi(2:n) = 283.0_fp
end function init_cond

function g_f(x, t) result(val)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(fp), intent(in) :: x, t
	real(fp) :: val 
	val = 0.0_fp
end function g_f

function s_f(t) result(val)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(fp), intent(in) :: t
	real(fp) :: val 
	val = 323.0_fp
end function s_f

function dsdt_f(t) result(val)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(fp), intent(in) :: t
	real(fp) :: val 
	val = 0.0_fp
end function dsdt_f

function dudx_f(t) result(val)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(fp), intent(in) :: t
	real(fp) :: val 
	val = 0.0_fp
end function dudx_f

function f_f(nx, us, xs, t, dx) result(fxs)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	integer(kind=16), intent(in) :: nx
	real(fp), intent(in) :: us(nx), xs(nx), t, dx 
	real(fp) :: fxs(nx), beta=8.2e-5_fp
	!attempt to do array slicing for efficiency
	fxs(1) = dsdt_f(t)
	fxs(2:nx-1) = beta / (dx * dx) * (us(3:nx) - 2.0_fp*us(2:nx-1) + us(1:nx-2)) !+ g_f(xs(2:nx-1), t) !am sad this slice doesn't work for some reason
	fxs(nx) = 2.0_fp*beta / (dx * dx) * (us(nx-1) + dx*dudx_f(t) - us(nx)) !+ g_f(dx * nx, t)
end function f_f

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Redu Precision Problem Functions
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
function g_r(x, t) result(val)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(rp), intent(in) :: x, t
	real(rp) :: val 
	val = 0.0_rp
end function g_r

function s_r(t) result(val)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(rp), intent(in) :: t
	real(rp) :: val 
	val = 323.0_rp
end function s_r

function dsdt_r(t) result(val)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(rp), intent(in) :: t
	real(rp) :: val 
	val = 0.0_rp
end function dsdt_r

function dudx_r(t) result(val)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(rp), intent(in) :: t
	real(rp) :: val 
	val = 0.0_rp
end function dudx_r

function f_r(nx, us, xs, t, dx) result(fxs)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	integer(kind=16), intent(in) :: nx
	real(rp), intent(in) :: us(nx), xs(nx), t, dx
	real(rp) :: fxs(nx), beta=8.2e-5_fp
	!attempt to do array slicing for efficiency
	fxs(1) = dsdt_r(t)
	fxs(2:nx-1) = beta / (dx * dx) * (us(3:nx) - 2.0_rp*us(2:nx-1) + us(1:nx-2)) !+ g_r(xs(2:nx-1), t)
	fxs(nx) = 2.0_rp*beta / (dx * dx) * (us(nx-1) + dx*dudx_r(t) - us(nx)) !+ g_r(dx * nx, t)
end function f_r

subroutine inverse(a,c,n)
!============================================================
! Inverse matrix
! Method: Based on Doolittle LU factorization for Ax=b
! Alex G. December 2009
! shamelessly borrowed from https://ww2.odu.edu/~agodunov/computing/programs/book2/Ch06/Inverse.f90
!-----------------------------------------------------------
! input ...
! a(n,n) - array of coefficients for matrix A
! n      - dimension
! output ...
! c(n,n) - inverse matrix of A
! comments ...
! the original matrix a(n,n) will be destroyed 
! during the calculation
!===========================================================
use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
implicit none 
integer n
real(rp) :: a(n,n), c(n,n)
real(rp) :: L(n,n), U(n,n), b(n), d(n), x(n)
real(rp) :: coeff
integer i, j, k

! step 0: initialization for matrices L and U and b
! Fortran 90/95 aloows such operations on matrices
L=0.0
U=0.0
b=0.0

! step 1: forward elimination
do k=1, n-1
   do i=k+1,n
      coeff=a(i,k)/a(k,k)
      L(i,k) = coeff
      do j=k+1,n
         a(i,j) = a(i,j)-coeff*a(k,j)
      end do
   end do
end do

! Step 2: prepare L and U matrices 
! L matrix is a matrix of the elimination coefficient
! + the diagonal elements are 1.0
do i=1,n
  L(i,i) = 1.0
end do
! U matrix is the upper triangular part of A
do j=1,n
  do i=1,j
    U(i,j) = a(i,j)
  end do
end do

! Step 3: compute columns of the inverse matrix C
do k=1,n
  b(k)=1.0
  d(1) = b(1)
! Step 3a: Solve Ld=b using the forward substitution
  do i=2,n
    d(i)=b(i)
    do j=1,i-1
      d(i) = d(i) - L(i,j)*d(j)
    end do
  end do
! Step 3b: Solve Ux=d using the back substitution
  x(n)=d(n)/U(n,n)
  do i = n-1,1,-1
    x(i) = d(i)
    do j=n,i+1,-1
      x(i)=x(i)-U(i,j)*x(j)
    end do
    x(i) = x(i)/u(i,i)
  end do
! Step 3c: fill the solutions x(n) into column k of C
  do i=1,n
    c(i,k) = x(i)
  end do
  b(k)=0.0
end do
end subroutine inverse


!!!!!!!!!!!!!!!!!!!!!!!!!
!Newtons method functions
!!!!!!!!!!!!!!!!!!!!!!!!!

!in the general case, this should depend on us, but for this
!problem there isn't a need
function jac(nx, dx, h) result(J)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	integer(kind=16), intent(in) :: nx
	real(rp), intent(in) :: dx, h
	real(rp) :: J(nx, nx), beta=8.2e-5_rp
	integer :: i 

	!jacobian for central difference discretization with impmid
	J = 0
	J(1, 1) = -1.0_rp
	do i=2,nx-1
		J(i, i-1) = h * beta / (2.0_rp * dx * dx)
		J(i, i) = -h * beta / (dx * dx) - 1.0_rp
		J(i, i+1) = h * beta / (2.0_rp * dx * dx)
	end do
	J(nx, nx-1) = h * beta / (dx * dx)
	J(nx, nx) = -h * beta / (dx * dx) - 1.0_rp
end function jac

subroutine newtons(nx, dx, t, h, xs, u, u0)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	integer(kind=16), intent(in) :: nx 
	real(rp), intent(in) :: dx, t, h
	real(rp), intent(in) :: xs(nx), u(nx)
	real(rp), intent(inout) :: u0(nx)
	real(rp) :: tol, norm
	real(rp) :: Fn(nx), Fni(nx), Jn(nx, nx), Jn_(nx, nx), Jni(nx, nx)
	integer :: i, max_iter=20

	tol = epsilon(tol) * 1.001_rp
	do i=0, max_iter-1
		Fn(:) = u(:) + h/2.0_rp * f_r(nx, u0, xs, t, dx) - u0(:)
		norm = norm2(Fn) !norm2 is an intrinsic function
		if (norm < tol) then
			return !converged so can leave
		end if 

		Jn = jac(nx, dx, h)
		call inverse(matmul(transpose(Jn), Jn), Jni, int(nx, 4)) !inverse expects an int not a long int
		Jn_ = matmul(Jni, transpose(Jn))
		Fni = matmul(Jn_, Fn)
		u0(:) = u0(:) - Fni(:)
	end do

end subroutine newtons

!method
subroutine imp_mid(ti, tf, dt, dx, nx, nt, us)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	integer(kind=16), intent(in) :: nt, nx
	real(fp), intent(in) :: ti, tf, dt, dx
	real(fp), intent(inout) :: us(2, nx)
	integer(kind=16) :: t, i, n_correct=1
	real(rp) :: t_cur
	real(rp) :: ui(nx), u(nx), xs_r(nx)
	real(fp) :: ue(nx), xs_f(nx)

	!initialize xs
	xs_f(1) = xi
	do i = 2, nx
		xs_f(i) = xi + i * dx
	end do 
	xs_r(1) = xi
	do i = 2, nx
		xs_r(i) = xi + i * dx
	end do 

	!initialize us 
	us(1, :) = init_cond(nx)
	t_cur = real(ti, rp)

	do t=1,nt 
		!print *, t
		ui(:) = real(us(1, :), rp) !convert from ys full precision to reduced
		u(:) = real(us(1, :), rp)

		t_cur = dt * t
		!stage 1
		call newtons(nx, real(dx, rp), t_cur, real(dt, rp), xs_r, u, ui) !nx, dx, t, h, xs, u, u0
		ue(:) = real(ui(:), fp) !up cast
		!stage 1 correction
		do i=1,n_correct
			ue(:) = us(1, :) + 0.5_fp * dt * f_f(nx, ue, xs_f, dt * t, dx)
		end do
		!update
		us(2, :) = us(1, :) + dt * f_f(nx, ue, xs_f, dt * t, dx)
		us(1, :) = us(2, :)

	end do
	!print *, t_cur

end subroutine imp_mid

end program main