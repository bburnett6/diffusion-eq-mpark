!gfortran sdirk.f08 -o sdirk -cpp -DFULL_TYPE=real128 -DREDU_TYPE=real32
! Full and reduced precision type defaults
#ifndef FULL_TYPE
#define FULL_TYPE real64
#endif
#ifndef REDU_TYPE
#define REDU_TYPE real32
#endif

program main
	!Declare the full and reduced precision type names.
	!remember for literal constants to put 1.0_fp to cast correctly
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	character(len=32) :: arg
	real(fp) :: dt, ti=0.0, tf=10.0, dx, xi=0.0, xf=0.5
	real :: start, finish !start/finish times for method
	real(fp), allocatable :: us(:, :)
	integer :: nt, nx

	!get command line args and then convert
	!https://gcc.gnu.org/onlinedocs/gfortran/GETARG.html
	!https://riptutorial.com/fortran/example/26615/passing-command-line-arguments
	call getarg(1, arg)
	read(arg, *)dt 
	!print *, dt
	call getarg(2, arg)
	read(arg, *)dx

	nx = (xf - xi) / dx 
	nt = (tf - ti) / dt 

	allocate(us(2, nx))
	
	call cpu_time(start)
	call rk4(ti, tf, dt, dx, nx, us)
	call cpu_time(finish)
	print *, "method duration: ", finish-start
	!printing the solution produces same final time solution 
	!as ref_solution.py with:
	!make fp=real64 rp=real64
	!./ref_solution.x 0.1 0.0125
	!print *, us(1, :)
	deallocate(us)


!Why this contains statement? see last answer and comments of
!https://stackoverflow.com/questions/16741252/function-return-type-mismatch
!links to useful
!http://www.pcc.qub.ac.uk/tec/courses/f77tof90/stu-notes/f90studentMIF_4.html
contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Full Precision Problem Functions
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
function init_cond(n) result(uxi)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	integer, intent(in) :: n
	real(fp) :: uxi(n)
	uxi(1) = 323.0_fp
	uxi(2:n) = 283.0_fp
end function init_cond

function g_f(x, t) result(val)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(fp), intent(in) :: x, t
	real(fp) :: val 
	val = 0.0_fp
end function g_f

function s_f(t) result(val)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(fp), intent(in) :: t
	real(fp) :: val 
	val = 323.0_fp
end function s_f

function dsdt_f(t) result(val)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(fp), intent(in) :: t
	real(fp) :: val 
	val = 0.0_fp
end function dsdt_f

function dudx_f(t) result(val)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(fp), intent(in) :: t
	real(fp) :: val 
	val = 0.0_fp
end function dudx_f

function f_f(nx, us, xs, t, dx) result(fxs)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	integer, intent(in) :: nx
	real(fp), intent(in) :: us(nx), xs(nx), t, dx 
	real(fp) :: fxs(nx), beta=8.2e-5_fp
	!attempt to do array slicing for efficiency
	fxs(1) = dsdt_f(t)
	fxs(2:nx-1) = beta / (dx * dx) * (us(3:nx) - 2.0_fp*us(2:nx-1) + us(1:nx-2)) !+ g_f(xs(2:nx-1), t) !am sad this slice doesn't work for some reason
	fxs(nx) = 2.0_fp*beta / (dx * dx) * (us(nx-1) + dx*dudx_f(t) - us(nx)) + g_f(dx * nx, t)
end function f_f

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Redu Precision Problem Functions
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
function g_r(x, t) result(val)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(rp), intent(in) :: x, t
	real(rp) :: val 
	val = 0.0_rp
end function g_r

function s_r(t) result(val)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(rp), intent(in) :: t
	real(rp) :: val 
	val = 323.0_rp
end function s_r

function dsdt_r(t) result(val)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(rp), intent(in) :: t
	real(rp) :: val 
	val = 0.0_rp
end function dsdt_r

function dudx_r(t) result(val)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(rp), intent(in) :: t
	real(rp) :: val 
	val = 0.0_rp
end function dudx_r

function f_r(nx, us, xs, t, dx) result(fxs)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	integer, intent(in) :: nx
	real(rp), intent(in) :: us(nx), xs(nx), t, dx
	real(rp) :: fxs(nx), beta=8.2e-5_fp
	!attempt to do array slicing for efficiency
	fxs(1) = dsdt_r(t)
	fxs(2:nx-1) = beta / (dx * dx) * (us(3:nx) - 2.0_rp*us(2:nx-1) + us(1:nx-2)) !+ g_r(xs(2:nx-1), t)
	fxs(nx) = 2.0_rp*beta / (dx * dx) * (us(nx-1) + dx*dudx_r(t) - us(nx)) + g_r(dx * nx, t)
end function f_r



subroutine inverse(a,c,n)
!============================================================
! Inverse matrix
! Method: Based on Doolittle LU factorization for Ax=b
! Alex G. December 2009
! shamelessly borrowed from https://ww2.odu.edu/~agodunov/computing/programs/book2/Ch06/Inverse.f90
!-----------------------------------------------------------
! input ...
! a(n,n) - array of coefficients for matrix A
! n      - dimension
! output ...
! c(n,n) - inverse matrix of A
! comments ...
! the original matrix a(n,n) will be destroyed 
! during the calculation
!===========================================================
use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
implicit none 
integer n
real(rp) :: a(n,n), c(n,n)
real(rp) :: L(n,n), U(n,n), b(n), d(n), x(n)
real(rp) :: coeff
integer i, j, k

! step 0: initialization for matrices L and U and b
! Fortran 90/95 aloows such operations on matrices
L=0.0
U=0.0
b=0.0

! step 1: forward elimination
do k=1, n-1
   do i=k+1,n
      coeff=a(i,k)/a(k,k)
      L(i,k) = coeff
      do j=k+1,n
         a(i,j) = a(i,j)-coeff*a(k,j)
      end do
   end do
end do

! Step 2: prepare L and U matrices 
! L matrix is a matrix of the elimination coefficient
! + the diagonal elements are 1.0
do i=1,n
  L(i,i) = 1.0
end do
! U matrix is the upper triangular part of A
do j=1,n
  do i=1,j
    U(i,j) = a(i,j)
  end do
end do

! Step 3: compute columns of the inverse matrix C
do k=1,n
  b(k)=1.0
  d(1) = b(1)
! Step 3a: Solve Ld=b using the forward substitution
  do i=2,n
    d(i)=b(i)
    do j=1,i-1
      d(i) = d(i) - L(i,j)*d(j)
    end do
  end do
! Step 3b: Solve Ux=d using the back substitution
  x(n)=d(n)/U(n,n)
  do i = n-1,1,-1
    x(i) = d(i)
    do j=n,i+1,-1
      x(i)=x(i)-U(i,j)*x(j)
    end do
    x(i) = x(i)/u(i,i)
  end do
! Step 3c: fill the solutions x(n) into column k of C
  do i=1,n
    c(i,k) = x(i)
  end do
  b(k)=0.0
end do
end subroutine inverse

subroutine rk4(ti, tf, dt, dx, nx, us)
	use, intrinsic :: iso_fortran_env, only: fp=>FULL_TYPE, rp=>REDU_TYPE
	implicit none
	real(fp), intent(in) :: ti, tf, dt, dx
	integer, intent(in) :: nx
	real(fp), intent(inout) :: us(2, nx)
	real(fp) :: k1(nx), k2(nx), k3(nx), k4(nx), tmp(nx), xs(nx)
	real(fp) :: h, t, te
	logical :: last_step
	integer :: i 

	!initialize xs
	xs(1) = xi
	do i = 2, nx
		xs(i) = xi + i * dx
	end do 
	!print *, xs(nx), xf

	last_step = .false.
	t = ti
	h = dt

	!initialize us 
	us(1, :) = init_cond(nx)

	open(unit=2, file='ref_sol.txt', ACTION="write", STATUS="replace")
	open(unit=12, file='ref_sol_t.txt', ACTION="write", STATUS="replace")
	!write(12,*) t
	!write(2,*) us(1, :)
	do while (t <= tf)
		if (t + h > tf) then 
			!make sure we do not go passed tf.
			h = tf - t 
			last_step = .true. 
		end if

		tmp(:) = us(1, :)
		k1(:) = f_f(nx, tmp, xs, t, dx)

		tmp(:) = us(1, :) + h * 0.5_fp * k1(:)
		k2(:) = f_f(nx, tmp, xs, t + 0.5_fp * h, dx)

		tmp(:) = us(1, :) + h * 0.5_fp * k2(:)
		k3(:) = f_f(nx, tmp, xs, t + 0.5_fp * h, dx)

		tmp(:) = us(1, :) + h * k3(:)
		k4(:) = f_f(nx, tmp, xs, t + h, dx)

		us(2, :) = us(1, :) + 1.0_fp/6.0_fp * h&
			* (k1(:) + 2.0_fp * k2(:) + 2.0_fp * k3(:) + k4(:)) 
 
		us(1, :) = us(2, :)
		!write(2,*) us(1, :)
		t = t + h
		!write(12,*) t

		if (last_step) then 
			exit 
		end if 
 
	end do 
	write(2,*) us(1, :)
	write(12,*) t
	close(2)
	close(12)

end subroutine rk4

end program main